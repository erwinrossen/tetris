# Youtube series

This repository is following along with the tutorial from the Tech With Tim series on Youtube.

- Part 1: https://www.youtube.com/watch?v=uoR4ilCWwKA  
- Part 2: https://www.youtube.com/watch?v=W8xCUu_zlQs
- Part 3: https://www.youtube.com/watch?v=HcGQB1nHOOM
- Part 4: https://www.youtube.com/watch?v=VgBnkkPlEVI

<img src="data/screenshot.png" alt="Screenshot" width="500"/>
