from typing import List, Tuple

import constants as c

# Types

Color = Tuple[int, int, int]
Position = Tuple[int, int]
Grid = List[List[Color]]


# Classes

class Piece:
    def __init__(self, x: int, y: int, shape: List[List[str]]):
        self.x = x
        self.y = y
        self.shape = shape
        self.color = c.shape_colors[c.shapes.index(shape)]
        self.rotation = 0
