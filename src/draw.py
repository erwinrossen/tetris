import pygame

from constants import *
from models import Piece, Grid, Color

pygame.font.init()
comic_sans_60 = pygame.font.SysFont('comicsans', 60)
comic_sans_30 = pygame.font.SysFont('comicsans', 30)
comic_sans_16 = pygame.font.SysFont('comicsans', 16)


def draw_text_middle(surface: pygame.Surface, text: str, size: int, color: Color) -> None:
    """
    Draw text in the middle of the screen
    """

    font = pygame.font.SysFont('comicsans', size, bold=True)
    label = font.render(text, True, color)
    top_left = (top_left_x + play_width / 2 - (label.get_width() / 2),
                top_left_y + play_height / 2 - label.get_height() / 2)
    surface.blit(label, top_left)


def draw_grid(surface: pygame.Surface):
    for y in range(top_left_y, top_left_y + play_height, block_size):
        pygame.draw.line(surface, (128, 128, 128), (top_left_x, y), (top_left_x + play_width, y))
        for x in range(top_left_x, top_left_x + play_width, block_size):
            pygame.draw.line(surface, (128, 128, 128), (x, top_left_y), (x, top_left_y + play_height))


def draw_next_shape(surface: pygame.Surface, piece: Piece):
    sx = top_left_x + play_width + 50  # 50 pixels to the right
    sy = top_left_y + play_height / 2 - 100  # 100 pixels above the middle

    label = comic_sans_30.render('Next Shape', True, (255, 255, 255))
    surface.blit(label, (sx + 10, sy - 60))

    shape = piece.shape[piece.rotation % len(piece.shape)]
    for i, line in enumerate(shape):
        row = list(line)
        for j, column in enumerate(row):
            if column == '0':
                rect = (sx + j * block_size,
                        sy + i * block_size,
                        block_size,
                        block_size)
                pygame.draw.rect(surface, piece.color, rect, width=0)


def draw_blocks(surface: pygame.Surface, grid: Grid):
    for i in range(nr_rows):
        for j in range(nr_cols):
            rect = (top_left_x + j * block_size,
                    top_left_y + i * block_size,
                    block_size,
                    block_size)
            pygame.draw.rect(surface, grid[i][j], rect, width=0)
    rect = (top_left_x, top_left_y, play_width, play_height)
    pygame.draw.rect(surface, (255, 0, 0), rect, width=4)


def draw_title(surface: pygame.Surface):
    label = comic_sans_60.render('Tetris', True, (255, 255, 255))
    surface.blit(label, (top_left_x + play_width / 2 - (label.get_width() / 2), 10))


def draw_score(surface: pygame.Surface, score: int):
    sx = top_left_x + play_width + 50  # 50 pixels to the right
    sy = top_left_y + play_height / 2 + 100  # 100 pixels below the middle

    label = comic_sans_16.render(f'Score: {score}', True, (255, 255, 255))
    surface.blit(label, (sx + 10, sy - 60))


def draw_high_score(surface: pygame.Surface, high_score: int):
    sx = top_left_x + play_width + 50  # 50 pixels to the right
    sy = top_left_y + play_height / 2 + 120  # 120 pixels below the middle

    label = comic_sans_16.render(f'High score: {high_score}', True, (255, 255, 255))
    surface.blit(label, (sx + 10, sy - 60))


def draw_window(surface: pygame.Surface, grid: Grid, next_piece: Piece, score: int, high_score: int) -> None:
    surface.fill((0, 0, 0))

    draw_title(surface)
    draw_blocks(surface, grid)
    draw_grid(surface)
    draw_next_shape(surface, next_piece)
    draw_score(surface, score)
    draw_high_score(surface, high_score)

    pygame.display.update()
