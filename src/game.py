import random
from operator import itemgetter
from typing import Dict, List

import pygame

import constants as c
from draw import draw_window, draw_text_middle
from models import Piece, Grid, Position, Color
from score import get_high_score, update_high_score, determine_score


def create_grid(locked_positions: Dict[Position, Color]) -> Grid:
    """
    Create the grid for the game

    :param locked_positions: Dictionary of occupied positions
    :return: The grid
    """

    grid = [[(0, 0, 0) for _ in range(c.nr_cols)] for _ in range(c.nr_rows)]

    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if (j, i) in locked_positions:
                grid[i][j] = locked_positions[(j, i)]

    return grid


def convert_shape_format(piece: Piece) -> List[Position]:
    """
    Convert the shape format to a list of positions

    :param piece: The piece to convert
    :return: A list of positions that this piece occupies
    """

    positions: List[Position] = []
    shape: List[str] = piece.shape[piece.rotation % len(piece.shape)]

    for i, line in enumerate(shape):
        row = list(line)
        for j, column in enumerate(row):
            if column == '0':
                positions.append((piece.x + j, piece.y + i))

    # Move everything left and up, because the shapes are not drawn in the top left corner
    for i, pos in enumerate(positions):
        positions[i] = (pos[0] - 2, pos[1] - 4)

    return positions


def valid_space(piece: Piece, grid: Grid) -> bool:
    """
    Check if the piece is inside the grid and not colliding with other pieces

    :param piece: The piece to check
    :param grid: The grid to check against
    :return: True if the piece is inside the grid and not colliding with other pieces
    """

    empty_positions = {(j, i)
                       for i in range(c.nr_rows)
                       for j in range(c.nr_cols)
                       if grid[i][j] == (0, 0, 0)}
    piece_positions = convert_shape_format(piece)
    for position in piece_positions:
        if position not in empty_positions and position[1] > -1:
            return False
    return True


def check_lost(positions: List[Position]) -> bool:
    """
    Check if the piece is above the top of the grid

    :param positions: List of occupied positions
    :return: True if the piece is above the top of the grid
    """

    for x, y in positions:
        if y < 1:
            return True
    return False


def get_piece() -> Piece:
    """
    Get a random piece

    :return: A random piece
    """

    return Piece(c.nr_cols // 2, 0, random.choice(c.shapes))


def clear_rows(grid: Grid, locked: Dict[Position, Color]) -> int:
    """
    Clear the rows that are full and move the other rows down

    :param grid: The grid to clear
    :param locked: The occupied positions
    :return: The number of rows that were cleared
    """

    nr_rows_cleared = 0
    index_of_cleared_row = None

    for i in range(len(grid) - 1, -1, -1):
        row = grid[i]
        if (0, 0, 0) not in row:
            nr_rows_cleared += 1
            index_of_cleared_row = i
            for j in range(len(row)):
                # try:
                del locked[(j, i)]
                # except:
                #     continue

    if index_of_cleared_row is not None:
        # Move the rows down from bottom to top,
        # otherwise the higher blocks might overwrite the lower blocks if you move them down
        for key in sorted(list(locked.keys()), key=itemgetter(1), reverse=True):
            x, y = key
            if y < index_of_cleared_row:
                # Only move the blocks down if they are above the cleared row
                new_key = (x, y + nr_rows_cleared)
                locked[new_key] = locked.pop(key)

    return nr_rows_cleared
