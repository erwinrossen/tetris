import os.path

import constants as c


def determine_score(nr_rows_cleared: int) -> int:
    """
    Determine the score based on the number of rows cleared

    The more rows you simultaneously clear, the more points you get per row

    :param nr_rows_cleared: The number of rows cleared
    :return: The score obtained from clearing the rows

    Example
    >>> determine_score(1)
    10
    >>> determine_score(2)
    28
    >>> determine_score(3)
    52
    >>> determine_score(4)
    80
    >>> determine_score(5)
    112
    """

    return int(round(c.nr_cols * nr_rows_cleared ** 1.5))


def update_high_score(last_score: int) -> None:
    """
    Update the high score if the last score is higher

    :param last_score: The last score
    """

    current_high_score = get_high_score()
    if last_score > current_high_score:
        with open(c.high_score_file, 'w') as f:
            f.write(str(last_score))


def get_high_score() -> int:
    """
    Get the high score from the high score file, or 0 if there is no high score file
    """

    if not os.path.isfile(c.high_score_file):
        return 0
    else:
        with open(c.high_score_file, 'r') as f:
            lines = f.readlines()
            if lines:
                return int(lines[0].strip())
            else:
                return 0
