import pygame

import constants as c
from draw import draw_window, draw_text_middle
from game import get_piece, create_grid, valid_space, convert_shape_format, clear_rows, check_lost
from score import get_high_score, update_high_score, determine_score


def main(surface: pygame.Surface):
    # Game flow variables
    game_running = True
    current_piece = get_piece()
    next_piece = get_piece()
    change_piece = False
    locked_positions = {}

    # Clock variables
    clock = pygame.time.Clock()
    fall_time = 0
    fall_speed = 0.27
    level_time = 0

    # Score variables
    score = 0
    current_high_score = get_high_score()

    while game_running:
        # Initialize the frame
        grid = create_grid(locked_positions)
        fall_time += clock.get_rawtime()  # in milliseconds
        level_time += clock.get_rawtime()
        clock.tick()

        # Gradually increase the speed of the game
        if level_time / 1000 > 5:
            if fall_speed > 0.1:
                fall_speed -= 0.005
            level_time = 0

        # Move the piece down automatically
        if fall_time / 1000 >= fall_speed:
            fall_time = 0
            current_piece.y += 1
            if not valid_space(current_piece, grid) and current_piece.y > 0:
                current_piece.y -= 1
                change_piece = True

        # Listen for key presses
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                update_high_score(score)
                game_running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q or event.key == pygame.K_ESCAPE:
                    update_high_score(score)
                    game_running = False
                elif event.key == pygame.K_LEFT:
                    current_piece.x -= 1
                    if not valid_space(current_piece, grid):
                        current_piece.x += 1
                elif event.key == pygame.K_RIGHT:
                    current_piece.x += 1
                    if not valid_space(current_piece, grid):
                        current_piece.x -= 1
                elif event.key == pygame.K_DOWN:
                    current_piece.y += 1
                    if not valid_space(current_piece, grid):
                        current_piece.y -= 1
                elif event.key == pygame.K_UP:
                    current_piece.rotation += 1
                    if not valid_space(current_piece, grid):
                        current_piece.rotation -= 1

        # Define the position of the current piece in the grid
        piece_positions = convert_shape_format(current_piece)
        for x, y in piece_positions:
            if y > -1:
                grid[y][x] = current_piece.color

        # Change the piece if it has reached the bottom
        if change_piece:
            for pos in piece_positions:
                p = (pos[0], pos[1])
                locked_positions[p] = current_piece.color
            current_piece = next_piece
            next_piece = get_piece()
            change_piece = False
            nr_rows_cleared = clear_rows(grid, locked_positions)
            score += determine_score(nr_rows_cleared)

        # Draw the window and everything in it
        draw_window(surface, grid, next_piece, score, current_high_score)

        # Check if the game is lost, end the game if it is
        if check_lost(list(locked_positions.keys())):
            draw_text_middle(surface, f'You lost!', size=80, color=(255, 255, 255))
            pygame.display.update()
            pygame.time.delay(1500)
            update_high_score(score)
            game_running = False


def main_menu(surface: pygame.Surface):
    main_menu_running = True

    while main_menu_running:
        surface.fill((0, 0, 0))
        draw_text_middle(surface, 'Press any key to play', size=60, color=(255, 255, 255))
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                main_menu_running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q or event.key == pygame.K_ESCAPE:
                    main_menu_running = False
                else:
                    main(surface)

    pygame.display.quit()


if __name__ == '__main__':
    window = pygame.display.set_mode((c.screen_width, c.screen_height))
    pygame.display.set_caption('Tetris')
    main_menu(window)
